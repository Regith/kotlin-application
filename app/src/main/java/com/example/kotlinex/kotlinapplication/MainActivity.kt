package com.example.kotlinex.kotlinapplication

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.example.kotlinex.kotlinapplication.common.Utility

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var mTextView: TextView
    val utility = Utility()
    lateinit var mButtonSave: Button
    lateinit var mButtonCancel: Button
    lateinit var mBundle : Button;
    val salary: String = "10000"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mTextView = findViewById(R.id.textView)
        //mTextView.setText("Success")
        mTextView.text = "Click"

        mTextView.setOnClickListener {
            utility.showToastMessage(applicationContext, "Clicked")
        }

        mButtonSave = findViewById(R.id.button1)
        mButtonSave.setOnClickListener {
            utility.showToastMessage(applicationContext, "Saved Successfully")
        }

        mButtonCancel = findViewById(R.id.button2)
        mButtonCancel.setOnClickListener(this)
        mBundle = findViewById(R.id.bundle)
        mBundle.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.button2 ->
                utility.showToastMessage(applicationContext, "Cancelled")
            R.id.bundle -> {
                // for pass value from one activity to another activity
                intent = Intent(this, NextActivity::class.java)
                intent.putExtra("name", "Regith")
                intent.putExtra("age", 28)
                intent.putExtra("salery", salary)
                startActivity(intent)
            }
        }
    }
}

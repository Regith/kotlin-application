package com.example.kotlinex.kotlinapplication

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView

class NextActivity : AppCompatActivity() {

    lateinit var mTextview : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.next_activity)

        mTextview = findViewById(R.id.textView)

        val bundle :Bundle = intent.extras
        val name = bundle.get("name")
        val age = bundle.get("age")
        val salery = bundle.get("salery");

        mTextview.text = "Name : "+name+ " Age : "+age+" Salary : "+salery;

    }
}
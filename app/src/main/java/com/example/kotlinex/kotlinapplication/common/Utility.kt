package com.example.kotlinex.kotlinapplication.common

import android.content.Context
import android.widget.Toast

class Utility {

    fun showToastMessage(context : Context,message : String){
        Toast.makeText(context,message,Toast.LENGTH_LONG).show();
    }


}